<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
//file_put_contents(getenv('OPENSHIFT_REPO_DIR').'crontest.txt', '1', FILE_APPEND);
include(getenv('OPENSHIFT_REPO_DIR').'BeerMan.php');
date_default_timezone_set('Asia/Novosibirsk');
DEFINE('LOG_FILE', getenv('OPENSHIFT_PHP_LOG_DIR').'crontask.log');
DEFINE('ACCOUNTS_FILE', getenv('OPENSHIFT_REPO_DIR').'accounts.ini');
DEFINE('SILENT', !false);

$beerMans=[]; //Array with all beerMans
$handle = fopen(ACCOUNTS_FILE, "r");
if ($handle) {
    while (($line = fgets($handle)) !== false) {
    	$account = explode(';', $line);
    	//print_r($account);
    	$man=new BeerMan(trim($account[0]), trim($account[1]));
        array_push($beerMans, $man);
    }
    fclose($handle);
} else {
    die("Error opening the file accounts.ini");
} 

foreach ($beerMans as &$man) {
	file_put_contents(LOG_FILE, "[".date('m/d/Y h:i:s a', time())."] ", FILE_APPEND);
	file_put_contents(LOG_FILE, $man->email.":\n", FILE_APPEND);

	$man->loginRequest(SILENT);

	$result=$man->bonus5minRequest(SILENT);
	file_put_contents(LOG_FILE,
		 '5m='.BeerMan::resultToString($result).", ", FILE_APPEND);

	$result=$man->bonus10minRequest(SILENT);
	file_put_contents(LOG_FILE,
		 '10m='.BeerMan::resultToString($result).", ", FILE_APPEND);

	$result=$man->bonus15minRequest(SILENT);
	file_put_contents(LOG_FILE,
		 '15m='.BeerMan::resultToString($result).", ", FILE_APPEND);

	$result=$man->bonus30minRequest(SILENT);
	file_put_contents(LOG_FILE,
		 '30m='.BeerMan::resultToString($result).", ", FILE_APPEND);

	$result=$man->bonus60minRequest(SILENT);
	file_put_contents(LOG_FILE,
		 '60m='.BeerMan::resultToString($result).", ", FILE_APPEND);

	$result=$man->bonusDailyRequest(SILENT);
	file_put_contents(LOG_FILE,
		 'daily='.BeerMan::resultToString($result).", ", FILE_APPEND);

	$result=$man->bonusVivodRequest(SILENT);
	file_put_contents(LOG_FILE,
		 'vivod='.BeerMan::resultToString($result)."\n", FILE_APPEND);
		 
}

?>

