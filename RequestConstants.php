<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

DEFINE('BONUS_RESULT_OK', 1);
DEFINE('BONUS_RESULT_FAILED', 0);
DEFINE('BONUS_RESULT_ERROR', -1);

class RequestConstants{

	public static $USER_AGENTS = array('Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13',
							  'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16',
							  'Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.14',
							  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
							  'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
							  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
							  'Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25'
							);   

	//ANSWERS
	const ANSWER_POSITIVE = '����� � �������';
	const ANSWER_NEGATIVE = '�� ��� �������� �����';

	//LOGIN
	const LOGIN_URL = 'http://beer-magnat.ru/index.php';   
	const LOGIN_TYPE = 'POST';

	//BONUS_5_MIN
	const BONUS_5_MIN_URL = 'http://beer-magnat.ru/account/bonus5min';   
	const BONUS_5_MIN_TYPE = 'POST';
	public static $BONUS_5_MIN_BODY = array("bonus" => "�������� �����" );

	//BONUS_10_MIN
	const BONUS_10_MIN_URL = 'http://beer-magnat.ru/account/bonus10min';   
	const BONUS_10_MIN_TYPE = 'POST';
	public static $BONUS_10_MIN_BODY = array("bonus" => "�������� �����" );

	//BONUS_15_MIN
	const BONUS_15_MIN_URL = 'http://beer-magnat.ru/account/bonus15min';   
	const BONUS_15_MIN_TYPE = 'POST';
	public static $BONUS_15_MIN_BODY = array("bonus" => "�������� �����" );

	//BONUS_30_MIN
	const BONUS_30_MIN_URL = 'http://beer-magnat.ru/account/bonus30min';   
	const BONUS_30_MIN_TYPE = 'POST';
	public static $BONUS_30_MIN_BODY = array("bonus" => "�������� �����" );

	//BONUS_60_MIN
	const BONUS_60_MIN_URL = 'http://beer-magnat.ru/account/bonus60min';   
	const BONUS_60_MIN_TYPE = 'POST';
	public static $BONUS_60_MIN_BODY = array("bonus" => "�������� �����" );

	//BONUS_DAILY
	const BONUS_DAILY_URL = 'http://beer-magnat.ru/account/bonus';   
	const BONUS_DAILY_TYPE = 'POST';
	public static $BONUS_DAILY_BODY = array("bonus" => "�������� �����" );

	//BONUS_VIVOD
	const BONUS_VIVOD_URL = 'http://beer-magnat.ru/account/bonusvivod';   
	const BONUS_VIVOD_TYPE = 'POST';
	public static $BONUS_VIVOD_BODY = array("bonus" => "�������� �����" );
}
?>